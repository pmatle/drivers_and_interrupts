// SPDX-License-Identifier: GPL-2.0
#ifndef DRIVERS_INTERRUPTS
# define DRIVERS_INTERRUPTS

# include <linux/module.h>
# include <linux/kernel.h>
# include <linux/init.h>
# include <linux/printk.h>
# include <linux/interrupt.h>
# include <linux/workqueue.h>
# include <linux/string.h>
# include <linux/slab.h>
# include <linux/fs.h>
# include <linux/miscdevice.h>
# include <linux/uaccess.h>
# include <linux/mutex.h>
# include <linux/input-event-codes.h>
# include <linux/time.h>
# include <linux/sort.h>
# include <linux/rtc.h>
# include <linux/jiffies.h>
# include <asm/io.h>
# include <asm/page.h>

# define KBD_PORT 1
# define KBD_STATUS_REG 0x64
# define KBD_DATA_REG 0x60
# define KBD_META "Meta_"
# define KBD_CTR "Control_"
# define KBD_CONSOLE "Console_"

struct special_keys
{
	unsigned char alt_active : 1;
	unsigned char alt : 1;
	unsigned char ctrl_active : 1;
	unsigned char control : 1;
	unsigned char caps_lock : 1;
	unsigned char shift_active : 1;
	unsigned char shift : 1;
};

struct dev_id
{
	char dev_name[16];
};

struct irq_sent_time
{
	suseconds_t	time_sec;
	int		year;
	int		month;
	int		day;
	int		hour;
	int		minute;
	int		second;
};

struct kbd_logger
{
	struct work_struct work;
	struct irq_sent_time time;
	unsigned int	scancode;
	unsigned int	status;
	unsigned int	work_count;
	unsigned int	combo: 1;
};

struct kbd_stats
{
	int		count;
	suseconds_t	start;
	suseconds_t	longest;
	char		keyname[32];
	unsigned char	pressed : 1;
	unsigned char	assigned : 1;
};

static char *keyname[119] = {"Escape", "One (1)|Exclamation (!)",
	"Two (2)|At (@)", "Three (3)|Number Sign (#)",
	"Four (4)|Dollar Sign ($)", "Five (5)|Percent (%)", "Six (6)|Caret (^)",
	"Seven (7)|Ampersant (&)", "Eight (8)|Asterick (*)",
	"Nine (9)|Parenthesis Left ('(')", "Zero (0) |Parenthesis Right (')')",
	"Dash (-)|Underscore (_)", "Equal (=)|Plus (+)", "Backspace", "Tab",
	"q|Q", "w|W", "e|E", "r|R", "t|T", "y|Y", "u|U", "i|I", "o|O", "p|P",
	"Bracket Left ([)|Brace Left ({)", "Bracket Right (])|Brace Right (})",
	"Return|Return (Numpad)", "Left Control|Right Control", "a|A", "s|S",
	"d|D", "f|F", "g|G", "h|H", "j|J", "k|K", "l|L",
	"Semi-Colon (;)|Colon (:)", "Apostrophe (\')|Double Quote (\")",
	"Grave (`)|Tilde (~)", "Left Shift", "Backslash (\\)|Bar", "z|Z",
	"x|X", "c|C", "v|V", "b|B", "n|N", "m|M", "Comma (,)|Less (<)",
	"Period (.)|Greater (>)", "Forward Slash (/)|Question Mark (?)",
	"Right Shift", "Multiply (*) (Keypad)|Print Screen",
	"Option or Alternative|Alternative Graph",
	"Space", "Caps Lock", "Function Key 1|Function Key 11",
	"Function Key 2|Function Key 12", "Function Key 3|Function Key 13",
	"Function Key 4|Function Key 14", "Function Key 5|Function Key 15",
	"Function Key 6|Function Key 16", "Function Key 7|Function Key 17",
	"Function Key 8|Function Key 18", "Function Key 9|Function Key 19",
	"Function Key 10|Function Key 20", "Number Lock",
	"Scroll Lock|Show Memery", "Seven (7) (Numpad)|Home",
	"Eight (8) (Numpad)|Up Arrow", "Nine (9) (Numpad)|Page Up",
	"Subtract (-) (Numpad)", "Four (4) (Numpad)|Left Arrow",
	"Five (5) (Numpad)", "Six (6) (Numpad)|Right Arrow", "Add (+) (Numpad)",
	"One (1) (Numpad)|End", "Two (2) (Numpad)|Down Arrow",
	"Three (3) (Numpad)|Page Down", "Zero (0) (Numpad)|Insert",
	"Period (.) (Numpad)|Delete", "Last Console", "", "Less (<)|Greater (>)"
	, "Function Key 11", "Function Key 12", "", "", "", "", "", "", "",
	"Enter (Numpad)", "Control", "Devide (/) (Numpad)", "Control Backslash",
	"Alternative Graph", "Break", "Home", "UP Arrow", "Page Up",
	"Left Arrow", "Right Arrow", "End", "Down Arrow", "Page Down", "Insert",
	"Remove", "Macro", "Function Key 13", "Function Key 14", "Help",
	"Do", "Function Key 17", "Minus/Plus (-/+) (Numpad)", "Pause"};

static char *keycode[95] = {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10",
	"11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22",
	"23", "24", "25", "26", "27", "28|96", "29|97", "30", "31", "32", "33",
	"34", "35", "36", "37", "38", "39", "40", "41", "42", "43", "44",
	"45", "46", "47", "48", "49", "50", "51", "52", "53|98", "54", "55|55",
	"56|100", "57", "58", "59|87", "60|88", "61", "62", "63", "64", "65",
	"66", "67", "68", "69", "70", "71|102", "72|103", "73|104", "74",
	"75|105", "76", "77|106", "78", "79|107", "80|108", "81|109",
	"82|110", "83|111", "84", "85", "86", "87", "88", "89", "90", "91",
	"92|126", "93", "94", "95"};

static struct special_keys	keys;
static struct kbd_stats		stats[130];
static struct kbd_stats		tmp[130];

static irqreturn_t	key_interrupt(int irg_no, void *dev_id);
static int	misc_dev_open(struct inode *, struct file *);
static int	misc_dev_release(struct inode *inode, struct file *file);
static ssize_t	misc_dev_read(struct file *fp, char *buff, size_t size,
		loff_t *offset);
static void		calculate_stats(int index, struct kbd_logger *logger);

#endif
