// SPDX-License-Identifier: GPL-2.0
#include "drivers_interrupts.h"

static				DEFINE_MUTEX(awesome_mutex);
static int			work_count;
static char			*file_contents;
static char			*contents;
static char			statistics[PAGE_SIZE];
static spinlock_t		lock;
static struct dev_id		*id;
static struct workqueue_struct	*kbd_workqueue;

/*
 * pr_info("First bit : %d\n", status & (1 << (0)));
 * pr_info("Second bit : %d\n", status & (1 << (1)));
 * pr_info("Third bit : %d\n", status & (1 << (2)));
 * pr_info("Fourth bit : %d\n", status & (1 << (3)));
 * pr_info("Fifth bit : %d\n", status & (1 << (4)));
 * pr_info("Sixth bit : %d\n", status & (1 << (5)));
 * pr_info("Seventh bit : %d\n", status & (1 << (6)));
 * pr_info("Eights bit : %d\n", (status & (1 << (7))));
 */

static char	*get_portion(char *str, int flag)
{
	char	*s1;
	char	*tmp;
	char	*ret;
	char	*holder;

	s1 = kstrdup(str, GFP_KERNEL);
	if (!s1)
		return NULL;
	holder = s1;
	tmp = strsep(&s1, "|");
	if (flag)
		tmp = strsep(&s1, "|");
	if (!tmp)
		goto end;
	ret = kstrdup(tmp, GFP_KERNEL);
	if (!ret)
		goto end;
	kfree(holder);
	return ret;
end:
	kfree(holder);
	return NULL;
}

static int	get_index(struct kbd_logger *logger)
{
	int	index;
	int	scancode;
	char	*s1;
	int	err;

	index = logger->scancode;
	scancode = logger->scancode;
	scancode -= (logger->scancode & 128) ? 129 : 1;
	s1 = get_portion(keycode[scancode], logger->combo);
	if (!s1) {
		s1 = (logger->combo) ? (strchr(keycode[scancode], '|') + 1) :
			keycode[scancode];
	}
	err = kstrtoint(s1, 10, &index);
	kfree(s1);
	if (err)
		return scancode;
	return index;
}

static int	append_key(char *entry, int len)
{
	char	*tmp;

	if (!file_contents) {
		file_contents = kzalloc(sizeof(*file_contents) * PAGE_SIZE,
					GFP_KERNEL);
		if (!file_contents)
			return 0;
	}
	if ((ksize(file_contents) - strlen(file_contents)) < len) {
		tmp = kzalloc(sizeof(*tmp) * (ksize(file_contents) + len),
			      GFP_KERNEL);
		if (!tmp)
			return 0;
		tmp = strncat(tmp, file_contents, strlen(file_contents));
		kfree(file_contents);
		file_contents = tmp;
	}
	file_contents = strncat(file_contents, entry, len);
	return 1;
}

int		get_entry_len(char *str, char *pre, char *post_pre,
			      char *key_num, struct kbd_logger *log)
{
	int	len;

	len = snprintf(NULL, 0, "%04d/%02d/%02d %02d:%02d:%02d %s%s%s(%s) %s%s"
		       , log->time.year, log->time.month,
		       log->time.day, log->time.hour,
		       log->time.minute, log->time.second,
		       pre, post_pre, str, key_num,
		       (log->scancode & 0x80) ? "Released" : "Pressed", "\n\n");
	return len;
}

static char	*get_keycode(int index, int combo)
{
	char	*s1;
	int	len;

	if (index < 95) {
		s1 = get_portion(keycode[index], combo);
	} else {
		s1 = kzalloc(sizeof(*s1) * 4, GFP_KERNEL);
		if (!s1)
			return NULL;
		len = snprintf(NULL, 0, "%d", index + 1);
		snprintf(s1, len, "%d", index + 1);
	}
	return s1;
}

static void	write_key(char *str, struct kbd_logger *log)
{
	int	index;
	char	*s1;
	int	len;
	char	tmp[1024];

	index = log->scancode;
	index -= (log->scancode & 0x80) ? 129 : 1;
	s1 = get_keycode(index, log->combo);
	if (!s1) {
		s1 = (log->combo) ? (strchr(keycode[index], '|') + 1) :
			keycode[index];
	}
	len = get_entry_len(str, "", "", s1, log);
	snprintf(tmp, len, "%04d/%02d/%02d %02d:%02d:%02d %s(%s) %s\n\n",
		 log->time.year, log->time.month,
		 log->time.day, log->time.hour,
		 log->time.minute, log->time.second, str, s1,
		 (log->scancode & 0x80) ? "Released" : "Pressed");
	append_key(tmp, len);
}

static void	write_console(int num, struct kbd_logger *log)
{
	int	index;
	int	len;
	char	tmp[1024];

	index = log->scancode;
	index -= (log->scancode & 0x80) ? 129 : 1;
	len = snprintf(NULL, 0, "%04d/%02d/%02d %02d:%02d:%02d %s%d(%s) %s\n\n",
		       log->time.year, log->time.month,
		       log->time.day, log->time.hour,
		       log->time.minute, log->time.second,
		       KBD_CONSOLE, num, keycode[index],
		       (log->scancode & 0x80) ? "Released" : "Pressed");
	snprintf(tmp, len, "%04d/%02d/%02d %02d:%02d:%02d %s%d(%s) %s\n\n",
		 log->time.year, log->time.month,
		 log->time.day, log->time.hour,
		 log->time.minute, log->time.second,
		 KBD_CONSOLE, num, keycode[index],
		 (log->scancode & 0x80) ? "Released" : "Pressed");
	append_key(tmp, len);
}

static void	write_prefix(char *prefix, char *str,
			     struct kbd_logger *log)
{
	char	*tmp;
	int	index;
	char	*s1;
	int	len;
	char	*key_num;
	char	key_entry[1024];

	s1 = kstrdup(str, GFP_KERNEL);
	if (!s1)
		write_key("Error on input", log);
	tmp = s1;
	strreplace(tmp, ' ', '_');
	index = log->scancode;
	index -= (log->scancode & 0x80) ? 129 : 1;
	key_num = get_keycode(index, log->combo);
	if (!key_num) {
		key_num = (log->combo) ? (strchr(keycode[index], '|') + 1) :
			keycode[index];
	}
	len = get_entry_len(str, prefix, "", key_num, log);
	snprintf(key_entry, len, "%04d/%02d/%02d %02d:%02d:%02d %s%s(%s) %s\n\n"
		 , log->time.year, log->time.month,
		 log->time.day, log->time.hour,
		 log->time.minute, log->time.second,
		 prefix, s1, key_num,
		 (log->scancode & 0x80) ? "Released" : "Pressed");
	append_key(key_entry, len);
	kfree(s1);
}

static void	write_combo(char *prefix, char *post_pre, char *str,
			    struct kbd_logger *log)
{
	char	*tmp;
	int	index;
	char	*s1;
	int	len;
	char	*key_num;
	char	entry[1024];

	s1 = kstrdup(str, GFP_KERNEL);
	if (!s1)
		write_key("Error on input", log);
	tmp = s1;
	strreplace(tmp, ' ', '_');
	index = log->scancode;
	index -= (log->scancode & 0x80) ? 129 : 1;
	key_num = get_keycode(index, log->combo);
	if (!key_num) {
		key_num = (log->combo) ? (strchr(keycode[index], '|') + 1) :
			keycode[index];
	}
	len = get_entry_len(str, prefix, post_pre, key_num, log);
	snprintf(entry, len, "%04d/%02d/%02d %02d:%02d:%02d %s%s%s(%s) %s\n\n"
		 , log->time.year, log->time.month,
		 log->time.day, log->time.hour,
		 log->time.minute, log->time.second,
		 prefix, post_pre, s1, key_num,
		 (log->scancode & 0x80) ? "Released" : "Pressed");
	append_key(entry, len);
	kfree(s1);
}

static void	special_key_stats(struct kbd_logger *logger)
{
	int	index;

	index = get_index(logger);
	calculate_stats(index - 1, logger);
}

static void	toggle_special_keys(struct kbd_logger *logger)
{
	int	state;
	int	scancode;

	scancode = logger->scancode;
	state = (logger->scancode & 128) ? 0 : 1;
	scancode -= (state) ? 0 : 128;
	if (!logger->combo && (scancode ==  KEY_LEFTSHIFT ||
			       scancode == KEY_RIGHTSHIFT)) {
		keys.shift = state;
		special_key_stats(logger);
		if (state == 0)
			keys.shift_active = 0;
	} else if (scancode == KEY_LEFTALT || scancode == KEY_RIGHTALT) {
		keys.alt = state;
		special_key_stats(logger);
		if (state == 0)
			keys.alt_active = 0;
	} else if (scancode == KEY_LEFTCTRL || scancode == KEY_RIGHTCTRL) {
		keys.control = state;
		special_key_stats(logger);
		if (state == 0)
			keys.ctrl_active = 0;
	} else if (!(logger->scancode & 128) && scancode == KEY_CAPSLOCK) {
		keys.caps_lock = (keys.caps_lock) ? 0 : 1;
		special_key_stats(logger);
	}
}

static void	process_normal(struct kbd_logger *logger)
{
	char	*token;
	int	index;
	int	state;

	index = logger->scancode;
	state = (logger->scancode & 128) ? 0 : 1;
	index -= (state) ? 1 : 129;
	if (((index == 28 || index == 96) && keys.ctrl_active) ||
	    ((index == 55 || index == 99) && keys.alt_active) ||
			((index == 41 || index == 53) && keys.shift_active))
		return;
	if (index == 91) {
		write_key("Command", logger);
		return;
	}
	if ((index > 88 && index < 96) || index > 119) {
		write_key("(Unknown Key)", logger);
		return;
	}
	token = get_portion(keyname[index], logger->combo);
	if (!token) {
		write_key(keyname[index], logger);
	} else {
		write_key(token, logger);
		kfree(token);
	}
}

static int	handle_caps_lock(struct kbd_logger *logger)
{
	int	index;
	char	*token;
	int	state;

	state = (logger->scancode & 0x80) ? 0 : 1;
	if (state)
		index = logger->scancode - 1;
	else
		index = logger->scancode - 129;
	if ((index >= 15 && index <= 24) || (index >= 29 && index <= 37) ||
	    (index >= 43 && index <= 49)) {
		token = get_portion(keyname[index], 1);
		if (!token) {
			write_key(keyname[index], logger);
		} else {
			write_key(token, logger);
			kfree(token);
		}
		return 1;
	}
	return 0;
}

static int	handle_shift(struct kbd_logger *logger)
{
	int	state;
	int	index;
	char	*token;

	state = (logger->scancode & 0x80) ? 0 : 1;
	if (state)
		index = logger->scancode - 1;
	else
		index = logger->scancode - 129;
	if (index == 41 || index == 54) {
		if (keys.shift_active == 0) {
			keys.shift_active = 1;
			write_key(keyname[index], logger);
		}
		return 1;
	}
	if (handle_caps_lock(logger))
		return 1;
	if ((index >= 1 && index <= 12) || (index >= 25 && index <= 26) ||
	    (index >= 38 && index <= 42) ||
			(index >= 50 && index <= 52) ||
			(index >= 58 && index <= 67)) {
		token = get_portion(keyname[index], 1);
		if (!token) {
			write_key(keyname[index], logger);
		} else {
			write_key(token, logger);
			kfree(token);
		}
		return 1;
	}
	return 0;
}

static int	handle_alt(struct kbd_logger *logger)
{
	char	*token;
	int	index;
	int	state;

	index = logger->scancode;
	state = (logger->scancode & 128) ? 0 : 1;
	index -= (state) ? 1 : 129;
	if (index == 55 || index == 99) {
		if (keys.alt_active == 0) {
			keys.alt_active = 1;
			token = get_portion(keyname[index], logger->combo);
			if (!token) {
				write_key(keyname[index], logger);
			} else {
				write_key(token, logger);
				kfree(token);
			}
		}
		return 1;
	}
	if ((index >= 58 && index <= 67) || (index >= 86 && index <= 87)) {
		index -= (index >= 58 && index <= 67) ? 57 : 75;
		write_console(index, logger);
		return 1;
	}
	if ((index >= 0 && index <= 26) || (index >= 29 && index <= 40) ||
	    (index >= 42 && index <= 52) || index == 56 ||
			index == 85) {
		token = get_portion(keyname[index], 0);
		if (!token) {
			write_prefix(KBD_META, keyname[index], logger);
		} else {
			write_prefix(KBD_META, token, logger);
			kfree(token);
		}
		return 1;
	}
	if (index == 104 || (logger->combo && index == 74)) {
		write_key("Decrement Console", logger);
		return 1;
	}
	if (index == 105 || (logger->combo && index == 76)) {
		write_key("Increment Console", logger);
		return 1;
	}
	if ((index >= 68 && index <= 82) || index == 98 || index == 108 ||
	    index == 103 || index == 110)
		process_normal(logger);
	return 1;
}

static void	control_special(int index, struct kbd_logger *logger)
{
	if (index == 2 || index == 56)
		write_key("nul", logger);
	else if (index == 3 || index == 25)
		write_key("Escape", logger);
	else if (index == 4)
		write_prefix(KBD_CTR, "Backslash", logger);
	else if (index == 5)
		write_prefix(KBD_CTR, "Bracket Right", logger);
	else if (index == 6)
		write_prefix(KBD_CTR, "Caret", logger);
	else if (index == 7 || index == 11)
		write_prefix(KBD_CTR, "Underscore", logger);
	else if (index == 22)
		write_key("Tab", logger);
	else if (index == 35)
		write_key("Line Feed", logger);
	else if (index == 51)
		write_key("Compose", logger);
	else if (index == 69)
		write_key("Show State", logger);
}

static void	control_alt_special(int index, struct kbd_logger *log)
{
	if (index == 22)
		write_prefix(KBD_META, "Tab", log);
	else if (index == 35)
		write_prefix(KBD_META, "Line Feed", log);
	else if (index == 34)
		write_prefix(KBD_META, "Backspace", log);
	else if (index == 110)
		write_key("Boot", log);
}

static int	handle_control(struct kbd_logger *logger)
{
	char	*token;
	int	index;
	int	state;

	index = logger->scancode;
	state = (logger->scancode & 128) ? 0 : 1;
	index -= (state) ? 1 : 129;
	if (index == 28 || index == 96) {
		if (keys.ctrl_active == 0) {
			keys.ctrl_active = 1;
			token = get_portion(keyname[index], logger->combo);
			if (!token) {
				write_key(keyname[index], logger);
			} else {
				write_key(token, logger);
				kfree(token);
			}
		}
		return 1;
	}
	if ((index >= 15 && index <= 21) || (index >= 23 && index <= 24) ||
	    index == 26 || (index >= 29 && index <= 33) ||
			(index >= 36 && index <= 37) ||
			(index >= 42 && index <= 49)) {
		token = get_portion(keyname[index], 0);
		if (!token) {
			write_prefix(KBD_CTR, keyname[index], logger);
		} else {
			write_prefix(KBD_CTR, token, logger);
			kfree(token);
		}
		return 1;
	}
	if (index == 8 || index == 13 || index == 34 || index == 52) {
		write_key("Backspace", logger);
	} else if ((index >= 2 && index <= 7) || index == 11 || index == 22 ||
			index == 25 || index == 35 || index == 40 ||
			index == 51 || index == 56 || index == 69) {
		control_special(index, logger);
	} else if ((index >= 58 && index <= 69) ||
			(index >= 70 && index <= 82) ||
			(index >= 86 && index <= 87) || index == 28 ||
			index == 14) {
		logger->combo = 0;
		process_normal(logger);
	}
	return 1;
}

static int	handle_alt_control(struct kbd_logger *logger)
{
	char	*token;
	int	index;
	int	state;

	index = logger->scancode;
	state = (logger->scancode & 128) ? 0 : 1;
	index -= (state) ? 1 : 129;
	if ((index >= 15 && index <= 21) || (index >= 23 && index <= 24) ||
	    index == 26 || (index >= 29 && index <= 33) ||
			(index >= 36 && index <= 37) ||
			(index >= 42 && index <= 49)) {
		token = get_portion(keyname[index], 0);
		if (!token) {
			write_combo(KBD_META, KBD_CTR, keyname[index], logger);
		} else {
			write_combo(KBD_META, KBD_CTR, token, logger);
			kfree(token);
		}
		return 1;
	}
	if ((index >= 58 && index <= 67) || (index >= 86 && index <= 87)) {
		index -= (index >= 58 && index <= 67) ? 57 : 75;
		write_console(index, logger);
		return 1;
	}
	if (index == 22 || index == 34 || index == 35 || index == 110)
		control_alt_special(index, logger);
	if ((index >= 70 && index <= 82) || (index >= 103 && index <= 105) ||
	    index == 68 || index == 27) {
		logger->combo = 0;
		process_normal(logger);
	}
	return 1;
}

static int	handle_shift_control(struct kbd_logger *logger)
{
	char	*token;
	int	index;
	int	state;

	index = logger->scancode;
	state = (logger->scancode & 128) ? 0 : 1;
	index -= (state) ? 1 : 129;
	if ((index >= 15 && index <= 21) || (index >= 23 && index <= 24) ||
	    index == 26 || (index >= 29 && index <= 33) ||
			(index >= 36 && index <= 37) ||
			(index >= 43 && index <= 49)) {
		token = get_portion(keyname[index], 0);
		if (!token) {
			write_prefix(KBD_CTR, keyname[index], logger);
		} else {
			write_prefix(KBD_CTR, token, logger);
			kfree(token);
		}
		return 1;
	} else if (index == 11 || index == 22 || index == 35) {
		control_special(index, logger);
	} else if ((index >= 70 && index <= 82) || index == 95 ||
			index == 97 || index == 68 || index == 54) {
		logger->combo = 0;
		process_normal(logger);
	}
	return 1;
}

static int	handle_special_keys(struct kbd_logger *logger)
{
	int	ret;

	ret = 0;
	if (keys.shift && keys.control && keys.alt)
		return 1;
	else if (keys.shift && keys.alt)
		return 1;
	else if (keys.shift && keys.control)
		ret = handle_shift_control(logger);
	else if (keys.alt && keys.control)
		ret = handle_alt_control(logger);
	else if (keys.shift)
		ret = handle_shift(logger);
	else if (keys.control)
		ret = handle_control(logger);
	else if (keys.alt)
		ret = handle_alt(logger);
	else if (keys.caps_lock)
		ret = handle_caps_lock(logger);
	return ret;
}

static void	assign_key_name(int index, int combo)
{
	char	*str;

	if (index == 125) {
		strncpy(stats[index].keyname, "Command", 7);
		stats[index].assigned = 1;
		return;
	}
	if (index > 118 || (index >= 88 && index <= 94) || index == 84) {
		strncpy(stats[index].keyname, "(Unkown Key)", 12);
		stats[index].assigned = 1;
		return ;
	}
	if (stats[index].assigned == 0) {
		str = get_portion(keyname[index], combo);
		if (!str) {
			strncpy(stats[index].keyname, keyname[index],
				strlen(keyname[index]));
		} else {
			strncpy(stats[index].keyname, str, strlen(str));
			kfree(str);
		}
		stats[index].assigned = 1;
	}
}

static void	calculate_stats(int index, struct kbd_logger *logger)
{
	suseconds_t	diff;

	if (index < 130) {
		if (logger->scancode & 128) {
			if (stats[index].start == 0)
				return;
			diff = logger->time.time_sec - stats[index].start;
			if (diff > stats[index].longest)
				stats[index].longest = diff;
			stats[index].pressed = 0;
		} else {
			stats[index].count++;
			if (stats[index].pressed == 0) {
				assign_key_name(index, logger->combo);
				stats[index].start = logger->time.time_sec;
				stats[index].pressed = 1;
			}
		}
	}
}

static void	get_stats(struct kbd_logger *logger)
{
	int	code;
	int	index;
	char	*s1;
	char	*tmp;
	int	err;

	code = logger->scancode;
	index = logger->scancode;
	index -= (code & 128) ? 129 : 1;
	if (index == 28 || index == 41 || index ==  53 || index == 55 ||
	    index == 96 || index == 99)
		return;
	if (index < 84 || (index >= 85 && index <= 87) || index == 91) {
		s1 = get_portion(keycode[index], logger->combo);
		if (!s1) {
			s1 = (logger->combo) ? (strchr(keycode[index], '|')
			      + 1) : keycode[index];
		}
		tmp = s1;
		index = 0;
		err = kstrtoint(tmp, 10, &index);
		if (!err)
			calculate_stats(index - 1, logger);
	}
}

static void	print_key(struct work_struct *work)
{
	struct kbd_logger	*logger;

	logger = (struct kbd_logger *)work;
	get_stats(logger);
	if (handle_special_keys(logger)) {
		kfree(logger);
		return;
	}
	process_normal(logger);
	kfree(logger);
}

static struct kbd_logger	*create_kbd_work(void)
{
	struct kbd_logger	*new_work;

	new_work = kmalloc(sizeof(*new_work), GFP_KERNEL | GFP_ATOMIC);
	if (new_work) {
		INIT_WORK((struct work_struct *)new_work, print_key);
		new_work->work_count = work_count;
		work_count++;
	}
	return new_work;
}

static struct irq_sent_time	get_irq_time(void)
{
	struct irq_sent_time	time;
	struct timeval		timeval;
	struct rtc_time		tm;
	unsigned long		localtime;

	do_gettimeofday(&timeval);
	localtime = (u32)(timeval.tv_sec - (sys_tz.tz_minuteswest * 60)); 
	rtc_time_to_tm(localtime, &tm);
	time.time_sec = ((jiffies * 1000) / HZ);
	time.year = tm.tm_year + 1900;
	time.month = tm.tm_mon + 1;
	time.day = tm.tm_mday;
	time.hour = tm.tm_hour;
	time.minute = tm.tm_min;
	time.second = tm.tm_sec;
	return time;
}

static void	handle_key(unsigned char scancode, unsigned char status,
			   unsigned int combo)
{
	struct kbd_logger	*logger;

	logger = create_kbd_work();
	if (logger) {
		logger->time = get_irq_time();
		logger->scancode = scancode;
		logger->status = status;
		logger->combo = combo;
		toggle_special_keys(logger);
		queue_work(kbd_workqueue, (struct work_struct *)logger);
	}
}

static int		fake_shift(int scancode, int flag)
{
	scancode -= (scancode & 128) ? 128 : 0;
	if (flag && (scancode ==  KEY_LEFTSHIFT || scancode == KEY_RIGHTSHIFT))
		return 1;
	return 0;
}

static irqreturn_t	key_interrupt(int irg_no, void *dev_id)
{
	struct dev_id		*id;
	unsigned char		status;
	unsigned char		scancode;
	static int		flag;

	id = (struct dev_id *)dev_id;
	if (strcmp((char *)id, "pmatle_keyboard"))
		return IRQ_NONE;
	spin_lock(&lock);
	status = inb(KBD_STATUS_REG);
	scancode = inb(KBD_DATA_REG);
	spin_unlock(&lock);
	if (scancode == 0xe0) {
		flag = 1;
	} else if (flag == 1) {
		if (fake_shift(scancode, flag)) {
			flag = 0;
			return IRQ_HANDLED;
		}
		handle_key(scancode, status, flag);
		flag = 0;
	} else {
		handle_key(scancode, status, flag);
	}
	return IRQ_HANDLED;
}

static int	compare_presses(const void *one, const void *two)
{
	struct kbd_stats	x;
	struct kbd_stats	y;

	x = *(struct kbd_stats *)one;
	y = *(struct kbd_stats *)two;

	if (x.count > y.count)
		return -1;
	else if (x.count < y.count)
		return 1;
	return 0;
}

static int	compare_time(const void *one, const void *two)
{
	struct kbd_stats	x;
	struct kbd_stats	y;

	x = *(struct kbd_stats *)one;
	y = *(struct kbd_stats *)two;

	if (x.longest > y.longest)
		return -1;
	else if (x.longest < y.longest)
		return 1;
	return 0;
}

static void	top_key_presses(void)
{
	int			x;
	int			len;
	char			str[100];

	x = 0;
	memcpy(tmp, stats, sizeof(stats));
	sort(tmp, 130, sizeof(tmp[0]), &compare_presses, NULL);
	strncat(statistics, "\n\n\t\tTop 5 Key Presses :\n\n", 25);
	strncat(statistics, "\tNum\t    Name\t\t\tPresses\n\n", 25);
	while (x < 5) {
		len = snprintf(NULL, 0, "\t%-7d\t    %-20s\t%-5d\n\n", x + 1,
			       tmp[x].keyname, tmp[x].count);
		snprintf(str, len, "\t%-7d\t    %-20s\t%-5d\n\n", x + 1,
			 tmp[x].keyname, tmp[x].count);
		strncat(statistics, str, len);
		x++;
	}
}

static void	top_time_pressed(void)
{
	int			x;
	int			len;
	char			str[100];

	x = 0;
	memcpy(tmp, stats, sizeof(stats));
	sort(tmp, 130, sizeof(tmp[0]), &compare_time, NULL);
	strncat(statistics, "\n\n\t\tTop 5 Longest Time Pressed :\n\n", 34);
	strncat(statistics, "\tNum\t    Name\t\t\tTime(msec)\n\n", 28);
	while (x < 5) {
		len = snprintf(NULL, 0, "\t%-7d\t    %-20s\t%-5ld\n\n", x + 1,
			       tmp[x].keyname, (tmp[x].longest / 1000));
		snprintf(str, len, "\t%-7d\t    %-20s\t%-5ld\n\n", x + 1,
			 tmp[x].keyname, tmp[x].longest);
		strncat(statistics, str, len);
		x++;
	}
}

static void	arrange_stats(void)
{
	int	x;
	int	len;
	char	str[100];
	
	x = 0;
	strncat(statistics, "\n\t    Statistics : \n\n", 21);
	strncat(statistics, "Num\tName\t\t    Presses\tTime(msec)\n", 33);
	while (x < 130) {
		if (!stats[x].longest && !stats[x].count) {
			x++;
			continue;
		}
		len = snprintf(NULL, 0, "%-3d\t%-20s%-5d\t%-10ld\n\n", x + 1,
			       stats[x].keyname, stats[x].count,
			       (stats[x].longest / 1000));
		snprintf(str, len, "%-3d\t%-20s%-5d\t%-10ld\n\n", x + 1,
			 stats[x].keyname, stats[x].count,
			 stats[x].longest);
		strncat(statistics, str, len);
		x++;
	}
	top_key_presses();
	top_time_pressed();
}

void			reset_stats(void)
{
	int	x;
	int	len;

	x = 0;
	while (x < 130) {
		len = sizeof(stats[x]);
		memset(&stats, '\0', len);
		x++;
	}
}

static const struct	file_operations fops = {
	.owner = THIS_MODULE,
	.read = misc_dev_read,
	.open = misc_dev_open,
	.release = misc_dev_release
};

static struct	miscdevice misc_dev_awesome = {
	.minor = MISC_DYNAMIC_MINOR,
	.name = "awesome",
	.fops = &fops,
	.mode = 0400
};

static int __init hello_init(void)
{
	int		err;

	id = kmalloc(sizeof(*id), GFP_KERNEL);
	memcpy(id->dev_name, "pmatle_keyboard\0", 16);
	err = request_irq(KBD_PORT, key_interrupt, IRQF_SHARED, "kbd_logger",
			  id);
	if (err)
		return err;
	kbd_workqueue = create_workqueue("kbd_workqueue");
	spin_lock_init(&lock);
	err = misc_register(&misc_dev_awesome);
	if (err) {
		pr_notice("failed to register driver awesome\n");
		return err;
	}
	reset_stats();
	mutex_init(&awesome_mutex);
	pr_info("Keyboard driver registered successfully!\n");
	return 0;
}

static int		misc_dev_open(struct inode *inode, struct file *file)
{
	int	len;

	if (!mutex_trylock(&awesome_mutex))
		return -EBUSY;
	arrange_stats();
	len = strlen(file_contents) + strlen(statistics) + 1;
	contents = kcalloc(len, sizeof(*contents), GFP_KERNEL);
	if (!contents)
		return -EFAULT;
	strncat(contents, file_contents, strlen(file_contents));
	strncat(contents, statistics, strlen(statistics));
	return 0;
}

static int		misc_dev_release(struct inode *inode, struct file *file)
{
	memset(statistics, '\0', PAGE_SIZE);
	kfree(contents);
	mutex_unlock(&awesome_mutex);
	return 0;
}

static ssize_t		misc_dev_read(struct file *fp, char *buff, size_t size,
				      loff_t *offset)
{
	int	ret;
	char	*tmp;

	if (!buff)
		return -EFAULT;
	tmp = contents;
	ret = simple_read_from_buffer(buff, size, offset, tmp, strlen(tmp));
	return ret;
}

static void __exit hello_exit(void)
{
	kfree(id);
	mutex_destroy(&awesome_mutex);
	flush_workqueue(kbd_workqueue);
	destroy_workqueue(kbd_workqueue);
	free_irq(1, id);
	pr_info("%s\n", file_contents);
	kfree(file_contents);
	misc_deregister(&misc_dev_awesome);
	pr_notice("Keyboard driver unregistered successfully.\n");
}

module_init(hello_init);
module_exit(hello_exit);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Phuti Matle <pmatle@student.wethinkcode.co.za>");
MODULE_DESCRIPTION("A keyboard driver that logs every key event on keyboard.");
