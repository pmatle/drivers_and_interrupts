# DRIVERS AND INTERRUPTS

The goal of this project is to create a keyboard driver that can register and log every key event on a keyboard. Basically a Keylogger.    
The driver must log all the key entries.  
A key entry should look like this:

* Key code
* State of the key (Pressed || Released)
* Name of the key. for example ’Return’ or ’Delete’.
* Ascii value of the key.
* Hour, minute and second when the IRQ is called.

Each key entry must be in one line like that:
```bash
HH:MM:SS Name of the key(key code) Pressed / Released
```
A misc device also need to be created and must be like /dev/module_keyboard, when read, the misc device should return the information saved.  
When the driver is destroyed, a piece of the stored information should be printed in a user friendly manner. This information must be printed in the kernel log file.

## Requirements
1. Linux Operating System (Any version and flavour)  
2. Admin rights

## Compilation

Use the Makefile provided to compile the executable for the program.

```bash
~$ make
```

## Usage

Use the following command to insert the module into the kernel:
```zsh
~$ insmod main.ko
```  
Use the following command to read the misc device after inserting the module to the kernel:
```bash
~$ less /dev/awesome
```  
Or you can use:
```bash
~$ cat /dev/awesome
```

Use the following command to remove the module from the kernel:
```bash
~$ rmmod main.ko
```

Use the following command after removing the module to see some or all the data gathered:
```bash
~$ dmesg | less
```

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

If you find any bugs while using the program please submit an issue so that the bug can be fixed.

## License
[MIT](https://choosealicense.com/licenses/mit/)